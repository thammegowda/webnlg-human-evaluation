#!/usr/bin/env python
#
# Author: Thamme Gowda [tg (at) isi (dot) edu] 
# Created: 10/15/20
import logging as log
from pathlib import Path
log.basicConfig(level=log.INFO)
import collections as coll
from csv import DictReader



def partition_groups(rows, mrs):
    mem = coll.defaultdict(dict)     # { team -> {id -> row}}
    # partition rows based on team names
    for row in rows:
        mem[row['team']][row['mr'].strip()] = row
    # sort rows of every team in the same order as given ids (so we can match with refs easily)
    for team_name, part in mem.items():
        assert len(part) == len(mrs), f'{team_name} expected {len(mrs)} but found {len(part)} ids'
        mem[team_name] = [part[mr] for mr in mrs]
    return mem

def write_teams(teams, out_dir, fields, delim='\t'):
    assert delim is not ' '
    for team_name, rows in teams.items():
        team_file = out_dir / (team_name + '.tsv')
        log.info(f'writing {team_file}')
        with team_file.open('w', encoding='utf-8', errors='ignore') as out:
            out.write(delim.join(fields) + '\n')
            for row in rows:
                line = delim.join(row[col].replace(delim, ' ').strip() for col in fields)
                out.write(line + '\n')

def main():
    inp = Path('all_data_final_averaged.csv')
    #ids_file = Path('sample-ids.txt') # this is useless / junk
    mrs_file = Path('MRs.txt')
    seg_scores_dir = Path('teams-seg-scores')
    corpus_scores = Path('tables/teams-corpus-human.tsv')

    seg_scores_dir.mkdir(exist_ok=True)
    #ids = [l.strip() for l in ids_file.read_text().strip().splitlines()]
    mrs = [l.strip() for l in mrs_file.read_text().strip().splitlines()]
    with inp.open(encoding='utf-8', errors='ignore') as f:
        rows = list(DictReader(f=f))
    log.info(f"read {len(rows)} rows from {inp}")
    teams = partition_groups(rows=rows, mrs=mrs)
    log.info(f"Found {len(teams)} teams")
    out_fields = 'text,fluency,grammar,semantics,bleu,meteor,ter,mr'.split(',')
    write_teams(teams, seg_scores_dir, fields=out_fields)

    write_corpus_scores(corpus_scores, teams)


def write_corpus_scores(file, teams,
                        fields ='fluency,grammar,semantics'.split(',')):
    # Corpus level score by taking avarge of segment scores

    team_summary = {}
    for team_name, rows in teams.items():
        team_summary[team_name] = {}
        for field in fields:
            scores = [float(row[field]) for row in rows]
            team_summary[team_name][field] = sum(scores) / len(scores)
    with file.open('w') as out:
        out.write("\t".join(['team'] + fields) + '\n')
        for team_name, avgs in team_summary.items():
            row = [team_name] + [f'{avgs[name]:.5f}' for name in fields]
            out.write('\t'.join(row) + '\n')


if __name__ == '__main__':
    main()
