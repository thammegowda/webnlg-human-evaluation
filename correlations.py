#!/usr/bin/env python
#
# Author: Thamme Gowda [tg (at) isi (dot) edu] 
# Created: 10/16/20


import logging as log
import pandas as pd

from pathlib import Path
from scipy.stats import kendalltau, pearsonr

log.basicConfig(level=log.INFO)


def main():

    tables_dir = Path('tables')
    humans = tables_dir / 'teams-corpus-human.tsv'
    auto = tables_dir / 'teams-corpus-autoeval.tsv'

    outliers = ['webnlg']  # webnlg is not a real system

    humans = pd.read_table(humans)
    auto = pd.read_table(auto)
    table = humans.merge(right=auto, on='team', how='inner')

    for outlier in outliers:
        table.drop(table[table['team'] == outlier].index, inplace=True)

    """
    for method in ['pearson', 'kendall']:
        corr_df = table.corr(method=method)
        # TODO: also print pval https://stackoverflow.com/questions/25571882/pandas-columns-correlation-with-statistical-significance

        out_file = corr_dir / f'{method}.tsv'
        print(f"====={out_file}====")
        print(corr_df)
        corr_df.to_csv(out_file, sep='\t', float_format='%.5f')
    """
    golds = 'fluency,grammar,semantics'.split(',')
    autos = 'bleu,chrf,macrof,microf,macrobleu,microbleu,bleurt_mean,bleurt_median'.split(',')

    head = ['Name', 'Correlation'] + golds
    res1 = []
    res2 = []
    for corr_name, corr_func in [('pearson', pearsonr), ('kendall', kendalltau)]:
        for auto in autos:
            row1 = [auto, corr_name]
            row2 = [auto, corr_name]
            for gold in golds:
                corr_val, p_val = corr_func(table[gold], table[auto])
                #print(corr_name, auto, gold, corr_val, p_val)
                row1.append(f'{corr_val:.3f}|{p_val:.2f}')
                row2.append(f'{corr_val:.3f}' + ('*' if p_val >= 0.05 else ''))
            res1.append(row1)
            res2.append(row2)
    data = res1 + res2

    out_f = tables_dir / 'correlations-all.csv'
    log.info(f'out_dir = {out_f}')
    df = pd.DataFrame(data, columns=head)
    df.to_csv(out_f)

if __name__ == '__main__':
    main()
