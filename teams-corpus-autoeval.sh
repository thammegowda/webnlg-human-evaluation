#!/usr/bin/env bash

export PYTHONPATH=../../libs/sacrebleu # Clone of https://github.com/mjpost/sacrebleu supports macrof microf macrobleu microbleu
#REFS=$(echo gold-sample-reference*.lex)
REF=gold-sample-reference0.lex

BLEURT=../../libs/bleurt

export bleurt_base="python -m bleurt.score -bleurt_checkpoint $BLEURT/bleurt-base-128 -average  "

mteval() {
  metrics="bleu chrf macrof microf macrobleu microbleu"
  echo "team $metrics bleurt_mean bleurt_median" | tr ' ' '\t'
  for sys in teams-seg-scores/*.tsv; do
    name=$(basename ${sys/.tsv/})
    printf "$name\t" && awk 'NR>1' $sys | cut -f1 | sacrebleu --force -b $REF -m $metrics | tr '\n' '\t' | gsed 's/\t$//'
    $bleurt_base -candidate_file <(awk -F '\t' '{ if(NR>1){ print $1 }}' $sys) -reference_file $REF | awk '{print "\t"$2"\t"$4}'
    #printf "\n"
    #break
  done
}

mteval 2> /dev/null
