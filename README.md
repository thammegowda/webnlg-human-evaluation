### WebNLG Human Evaluation
The repository contains files used for human evaluation in the [WebNLG Challenge](https://webnlg-challenge.loria.fr/challenge_2017/#challenge-results).

More details can be found in the [report](https://webnlg-challenge.loria.fr/files/human-eval-outline-v2.pdf).

We sampled 223 (data, text) pairs for human evaluation.

* _sample-ids.txt_ contains sampled ids from test set
* _MRs.txt_ contains data
* _gold-sample-reference*.lex_ contains references
* _all\_data\_final\_averaged.csv_ contains human evaluation results. Fluency, Semantics, and Grammar columns represent average scores that we got by collecting three human judgments. References are denoted by _webnlg_; their automatic scores are always 1.0 or 0.0.
* _all\_data\_final\_scores\_anonymised.csv_ contains raw human scores. 

To replicate the results, run `human-evaluation.R`.

Results obtained in this study also gave rise to the following publication:

Anastasia Shimorina. _Human vs Automatic Metrics: on the Importance of Correlation Design_. WiNLP Workshop at NAACL, 2018. [[pdf](https://arxiv.org/abs/1805.11474)] [[poster](winlp18_poster.pdf)]

----

Repository was originally at  https://gitlab.com/webnlg/webnlg-human-evaluation.git  

> These changes are made by Thamme Gowda [tg at isi.edu] for his own research

* `csvtoplain.tsv` - creates `teams-seg-scores/*.tsv` and `tables/teams-corpus-scores.tsv` corpus scores is simple average of segment scores 
* `teams-seg-scores/*.tsv` are system outputs from each team including their segment level scores
  * Matching references are  `old-sample-reference*.lex`
* `teams-corpus-humans.tsv` corpus level human judgement scores for fluency, grammar, semantics 
obtained by averaging segment level scores
*  `teams-corpus-autoeval.sh > tables/teams-corpus-autoeval.tsv`  get autoeval scores (using sacrebleu) 
* `./corelations.py` produces `correlations/<x>.tsv` where x \in { pearson, spearman, and kendall}  
 
